

/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/


class YaRRStrings
{
public:
	static char *Strings[100];
	static void Startup();
	static void Shutdown();
	static const char *Fetch(int sID);
};

#define GAME_ARGS 1
#define GAME_PNOTFIND 2
#define GAME_PFMANY 3
#define IRC_ARGS 4
#define IRC_PNOTFIND 5
#define IRC_PFMANY 6
#define IRC_CHANNEL_JOIN 7
#define GAME_NSTATUS 8
#define IRC_NSTATUS 9
#define GAME_NCOM 10
#define IRC_NCOM 11
#define IRC_AUTHNSTATUS 12
#define IRC_NMOD 13
#define IRC_EFIG 14
#define IRC_DTJLM 15
#define IRC_JLM 16
#define IRC_TAJG 17
#define IRC_DTJLMURS 18
#define IRC_JLMURS 19