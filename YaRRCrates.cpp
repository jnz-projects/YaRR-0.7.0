#include "YaRRIncludes.h"


/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/



Stacker<Crate> *YaRRCrate::Crates = 0;
Stacker<Vector3> *YaRRCrate::Spawns = 0;
CRITICAL_SECTION YaRRCrate::Mutex;
bool YaRRCrate::IsActive;

void YaRRCrate::Startup()
{
	DLOG;
	Crates = Alloc(Stacker<Crate>);
	Spawns = Alloc(Stacker<Vector3>);
	InitializeCriticalSection(&Mutex);

	YaRRCrate::Lock();

	IterateStack(x, Crate, YaRRSettings::Crates)
	{
		Crates->Push(x);
	}
}

void YaRRCrate::Shutdown()
{
	DLOG;
	Dealloc(Stacker<Crate>, YaRRCrate::Crates);
	Dealloc(Stacker<Vector3>, Spawns);
	DeleteCriticalSection(&YaRRCrate::Mutex);
}

void YaRRCrate::ObjectCreated(GameObject *obj)
{
	DLOG;
	if(Is_Powerup(obj))
	{
		const char *p = Commands->Get_Preset_Name(obj);
		if(stristr(p, "crate"))
		{
			Vector3 pos = Commands->Get_Position(obj);
			bool exist = 0;
			YaRRCrate::Lock();
			IterateStack(x, Vector3, Spawns)
			{
				if(pos.X == x.X && pos.Y == x.Y && pos.Z == x.Z)
				{
					exist = 1;
					break;
				}
			}
			if(!exist)
			{
				Spawns->Push(pos);
			}
			YaRRCrate::Unlock();

			Commands->Destroy_Object(obj);
		}
	}
}

unsigned YaRRCrate::Timer(void *)
{
	DLOG;
	YaRRCrate::Lock();
	
	if(!YaRRCrate::IsActive)
	{
		Vector3 pos = *Spawns->At(Commands->Get_Random_Int(0, Spawns->Length()));
		GameObject *crate = Commands->Create_Object("POW_Data_Disc", pos);
		Commands->Set_Model(crate, YaRRSettings::CrateModel);
		Set_Is_Powerup_Persistant(crate, 1);
		Set_Powerup_Always_Allow_Grant(crate, 1);
		int Blocker = Commands->Get_ID(Commands->Create_Object("Vehicle_Blocker", pos));
		char p[16];
		sprintf(p, "%d", Blocker);
		Attach_Script_Once(crate, "CrateScript", p);
		IsActive = 1;
	}

	YaRRCrate::Unlock();

	return 1000;
}

void YaRRCrate::Lock()
{
	DLOG;
	EnterCriticalSection(&Mutex);
}

void YaRRCrate::Unlock()
{
	DLOG;
	Sleep(0);
	LeaveCriticalSection(&Mutex);
}

void YaRRCrate::Test(Player *p)
{
	DLOG;
	YaRRFunctions::PPage(p->ID, "Congratulations, you picked up the crate");
}


void YaRRCrate::CrateScript::Created()
{
	DLOG;
	
}

void YaRRCrate::CrateScript::Custom(GameObject *obj, int message, int param, GameObject *sender)
{
	DLOG;
	if(message != 1000000025)
	{
		return;		
	}

	Player *p = YaRRPlayers::Find(Get_Player_ID(sender));
	if(!p)
	{
		return;
	}

	YaRRCrate::Lock();

	int cratenum = Commands->Get_Random_Int(0, 101);
	
	IterateStack(c, Crate, YaRRCrate::Crates)
	{
		if(cratenum > c.ChanceDN && cratenum < c.ChanceUP)
		{
			c.Activate(p);
		}
	}

	Commands->Destroy_Object(obj);
	IsActive = 0;

	YaRRCrate::Unlock();
}

ScriptRegistrant<YaRRCrate::CrateScript> CrateScriptRegistrant("CrateScript", "Blocker:int");