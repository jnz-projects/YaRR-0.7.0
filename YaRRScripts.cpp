#include "YaRRIncludes.h"


/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/


int IRCThinkScript::sfps = 0;

void IRCThinkScript::Created(GameObject *o)
{
	DLOG;
	extrad = 0;
	ReconSent = 0;
	lastframecount = 0;
	Commands->Start_Timer(o, this, 0.2, 10);
	Commands->Start_Timer(o, this, 1.0, 12);
	Commands->Start_Timer(o, this, 5.0, 13);
	Commands->Start_Timer(o, this, 5.0, 14);
	Commands->Start_Timer(o, this, 1.0, 15);
	Commands->Start_Timer(o, this, 2.0, 16);
	if(!YaRRSettings::AnnounceList->Empty())
	{
		Commands->Start_Timer(o, this, (float)YaRRSettings::AnnounceTimeout, 11);
	}
	if(YaRRGlobal::IRC)
	{
		YaRRGlobal::IRC->ThinkScript = Commands->Get_ID(o);
	}
}

void IRCThinkScript::Timer_Expired(GameObject *o, int number)
{
	DLOG;
	if(number == 10)
	{
		IRC::Think(o);
		Commands->Start_Timer(o, this, 0.1, 10);
	}
	else if(number == 11)
	{
		if(The_Game()->CurrentPlayers != 0)
		{
			char *tmp;
			if(!YaRRSettings::AnnounceList->Iterate(&tmp))
			{
				YaRRSettings::AnnounceList->Reset();
				YaRRSettings::AnnounceList->Iterate(&tmp);
			}
			ConsoleInputF("msg %s", tmp);
		}
		Commands->Start_Timer(o, this, (float)YaRRSettings::AnnounceTimeout, 11);
	}
	else if(number == 12)
	{
		sfps = The_Game()->FrameCount-lastframecount;
		lastframecount = The_Game()->FrameCount;
		Commands->Start_Timer(o, this, 1.0, 12);
	}
	else if(number == 13)
	{
		if(sfps < 10)
		{
			IRC::SendC('a', "%c4[Warning]%c: Severe SFPS (%d).", 3, 3, sfps);  
			Commands->Start_Timer(o, this, 60.0, 13);
		}
		Commands->Start_Timer(o, this, 5.0, 13);

	}
	else if(number == 14)
	{	
		ConsoleInputF("msg Extra base defences %s", extrad == 0 ? "disabled" : "enabled");
	}
	else if(number == 15)
	{
		if(The_Game()->CurrentPlayers == 0 && !YaRRGlobal::NoOutput)
		{
			IRC::SendC('b', "\x03%dNo players in-game, stopping IRC output.", 3);
			YaRRGlobal::NoOutput = 1;
		}
		else if(YaRRGlobal::NoOutput && The_Game()->CurrentPlayers != 0)
		{
			IRC::SendC('b', "\x03%dPlayer joined, restarting output. Map is: %s", 3, The_Game()->MapName);
			YaRRGlobal::NoOutput = 0;
		}

		Commands->Start_Timer(o, this, 1.0, 15);
	}
	else if(number == 16)
	{
		YaRRVeteran::Think();
		Commands->Start_Timer(o, this, 2.0, 16);
	}
	else if(number == 17)
	{
		if(ReconSent)
		{
			return;
		}
		printf("Disconnected from the IRCd. Attempting to reconnect.\n");
		ReconSent = 1;
		Commands->Start_Timer(o, this, 30.0, 18);
	}
	else if(number == 18)
	{
		if(YaRRGlobal::IRC->Sock.Client(YaRRGlobal::IRC->Host, YaRRGlobal::IRC->Port))
		{
			printf("Reconnected to %s\n", YaRRGlobal::IRC->Host);
			ReconSent = 0;
			IRC::Send("NICK %s\n", YaRRGlobal::IRC->Nick);
			IRC::Send("USER %s 0 * :YaRR-%s\n", YaRRGlobal::IRC->Nick, YARR_VERSION);
		}
		else
		{
			Commands->Start_Timer(o, this, 30.0, 18);
		}
	}	
}

void IRCThinkScript::Custom(GameObject *o, int message, int param, GameObject *sender)
{
	DLOG;
	
	if(message == 100)
	{
		extrad = 1;
	}
	else if(message == 101)
	{
		extrad = 0;
	}
	if(message == 102)
	{
		Commands->Start_Timer(o, this, 0.0, 17);
	}
}

ScriptRegistrant<IRCThinkScript> IRCThinkScriptRegistrant("IRCThinkScript", "");


void YaRRboot::Created(GameObject *o)
{
	DLOG;
	Commands->Start_Timer(o, this, 1.0, 1);
	Commands->Start_Timer(o, this, 1.5, 2);
}

void YaRRboot::Timer_Expired(GameObject *o, int number)
{
	DLOG;
	if(number == 1)
	{
		ConsoleInputF("pamsg %d %s", Get_Int_Parameter("ID"), Get_Parameter("reason"));
	}
	else if(number == 2)
	{
		const char *pName = Get_Player_Name_By_ID(Get_Int_Parameter("ID"));
		ConsoleInputF("kick %d", Get_Int_Parameter("ID"));
		ConsoleInputF("allow %s", pName);
		delete []pName;
		Commands->Destroy_Object(o);
	}
}

ScriptRegistrant<YaRRboot> YaRRbootRegistrant("YaRRBoot", "ID:int,reason:string");


void YaRRPlayer::Created(GameObject *o)
{
	DLOG;
}

void YaRRPlayer::Timer_Expired(GameObject *o, int number)
{
	DLOG;

}

void YaRRPlayer::Damaged(GameObject *o, GameObject *attacker, float damage)
{
	DLOG;
	if(!attacker || !o || (o == attacker) || !Commands->Is_A_Star(attacker) || (damage == 0))
	{
		return;
	}
	Player *p = Find(Get_Player_ID(attacker));
	if(!p)
	{
		return;
	}
	int t = Get_Object_Type(o);
	if(t == p->data->PlayerType.Get() || t < 0 || t > 1)
	{
		return;
	}
	GameObject *veh = Get_Vehicle(attacker);
	float AtkCost = (float)GetWorth(veh == 0 ? Commands->Get_Preset_Name(attacker) : Commands->Get_Preset_Name(veh));
	float VicCost = (float)GetWorth(Commands->Get_Preset_Name(o));

	AtkCost += (AtkCost == 0 ? 1 : 0);
	VicCost += (VicCost == 0 ? 1 : 0);

	if(AtkCost < VicCost)
	{
		AtkCost += (float)((VicCost-AtkCost)*0.5);
	}

	if(damage > 0)
	{
		p->Rank.Player_Damage += (VicCost / AtkCost) * RankInfo::Player_Damage_CoEfficient * damage;
	}
	else
	{
		p->Rank.Player_Repair += RankInfo::Player_Repair_CoEfficient * damage;
	}
	YaRRVeteran::ObjectDamaged(p->ID, o, damage);
}

void YaRRPlayer::Killed(GameObject *o, GameObject *Killer)
{
	DLOG;
	char victimw[256];
	char killerw[256];

	if(Commands->Is_A_Star(Killer))
	{
		
		GetWeaponString(o, victimw, 256);
		GetWeaponString(Killer, killerw, 256);
	
		bool IsSniper = 0;
		const char *Preset = Get_Current_Weapon(Killer);
		if(Preset)
		{
			if(strcmp(Preset, "Weapon_RamjetRifle_Player") == 0)
			{
				IsSniper = 1;
			}
			else if(strcmp(Preset, "Weapon_SniperRifle_Player") == 0)
			{
				IsSniper = 1;
			}
		}
		bool whs = YaRRPlayers::WasHeadShot(Get_Player_ID(Killer));


		IRC::SendC('b', "\x03%d%S(%s) killed %S(%s) %s\x03\n", Get_Team(Get_Player_ID(Killer)) == 1 ? 8 : 4, Get_Wide_Player_Name(Killer), killerw, Get_Wide_Player_Name(o), victimw, (IsSniper == 1 ? (whs == 1 ? "-(*_*)->" : "") : ""));
		YaRRVeteran::Kill(o, Get_Player_ID(Killer));
		YaRRVeteran::Killed(Killer, Get_Player_ID(o));
		YaRRVeteran::ObjectKilled(Get_Player_ID(Killer), o);
	}
	else if(o == Killer)
	{
		IRC::SendC('b', "\x03%d%S(%s) committed suicide.", Get_Team(Get_Player_ID(o)) == 1 ? 4 : 8, Get_Wide_Player_Name(o), GetWeaponString(o, victimw, 256));
	}
	else if(Is_Vehicle(Killer))
	{
		const char *bld = GetPresetScreenName(Killer);
		IRC::SendC('b', "\x03%d%S(%s) was killed by the %s\n", Get_Team(Get_Player_ID(o)) == 1 ? 4 : 8, Get_Wide_Player_Name(o), GetWeaponString(o, victimw, 256), bld);
		CDealloc(bld);
	}
	else
	{
		IRC::SendC('b', "\x03%d%S(%s) has died.", Get_Team(Get_Player_ID(o)) == 1 ? 4 : 8, Get_Wide_Player_Name(o), GetWeaponString(o, victimw, 256));
	}
	
}

void YaRRPlayer::Destroyed(GameObject *o)
{
	DLOG;

}

ScriptRegistrant<YaRRPlayer> YaRRPlayerRegistrant("YaRRPlayer", "");


void YaRRVehicle::Created(GameObject *o)
{
	DLOG;

}

void YaRRVehicle::Timer_Expired(GameObject *o, int number)
{
	DLOG;

}

void YaRRVehicle::Damaged(GameObject *o, GameObject *attacker, float damage)
{
	DLOG;
	if(!attacker || !o || (o == attacker) || !Commands->Is_A_Star(attacker) || (damage == 0))
	{
		return;
	}
	Player *p = Find(Get_Player_ID(attacker));
	if(!p)
	{
		return;
	}
	int t = Get_Object_Type(o);
	if(t == p->data->PlayerType.Get() || t < 0 || t > 1)
	{
		return;
	}
	GameObject *veh = Get_Vehicle(attacker);
	float AtkCost = (float)GetWorth(veh == 0 ? Commands->Get_Preset_Name(attacker) : Commands->Get_Preset_Name(veh));
	float VicCost = (float)GetWorth(Commands->Get_Preset_Name(o));

	AtkCost += (AtkCost == 0 ? 1 : 0);
	VicCost += (VicCost == 0 ? 1 : 0);

	if(AtkCost < VicCost)
	{
		AtkCost += (float)((VicCost-AtkCost)*0.5);
	}

	if(damage > 0)
	{
		p->Rank.Vehicle_Damage += (VicCost / AtkCost) * RankInfo::Vehicle_Damage_CoEfficient * damage;
	}
	else
	{
		p->Rank.Vehicle_Repair += RankInfo::Vehicle_Repair_CoEfficient * damage;
	}
	YaRRVeteran::ObjectDamaged(p->ID, o, damage);
}

void YaRRVehicle::Killed(GameObject *o, GameObject *killer)
{
	DLOG;
	if(killer)
	{
		if(Commands->Is_A_Star(killer))
		{
			
			if(!Get_Vehicle_Driver(o))
			{
				char killerw[256];
				GetWeaponString(killer, killerw, 256);
				const char *objp = GetPresetScreenName(o);

				IRC::SendC('b', "\x03%d%S(%s) destroyed a %s\x03\n", Get_Team(Get_Player_ID(killer)) == 1 ? 8 : 4, Get_Wide_Player_Name(killer), killerw, objp);
				CDealloc(objp);
			}
			else
			{
				YaRRVeteran::Kill(o, Get_Player_ID(killer));
				YaRRVeteran::Killed(killer, Get_Player_ID(Get_Vehicle_Driver(o)));
				YaRRVeteran::ObjectKilled(Get_Player_ID(killer), o);
				char killerw[256];
				GetWeaponString(killer, killerw, 256);
				const char *pName = Get_Player_Name(Get_Vehicle_Driver(o));
				const char *objp = GetPresetScreenName(o);

				IRC::SendC('b', "\x03%d%S(%s) destroyed %s%s %S\x03\n", Get_Team(Get_Player_ID(killer)) == 1 ? 8 : 4, Get_Wide_Player_Name(killer), killerw, pName, tolower(pName[strlen(pName)-1]) == 's' ? "'" : "'s", objp);
				delete []pName;
				CDealloc(objp);
			}
		}
		else if(Is_Vehicle(killer))
		{
			const char *killerp = GetPresetScreenName(killer);
			const char *objp = GetPresetScreenName(o);

			IRC::SendC('b', "\x03%dA %s destroyed a %s\n", Get_Object_Type(killer) == 1 ? 8 : 4, killerp, objp);
			CDealloc(killerp);
			CDealloc(objp);
		}
	}
	else
	{
		const char *objp = GetPresetScreenName(o);
		IRC::SendC('b', "\x03%dA %s was destroyed\n", Get_Object_Type(o) == 0 ? 8 : 4, objp);
		CDealloc(objp);
	}
}

void YaRRVehicle::Destroyed(GameObject *o)
{
	DLOG;

}

ScriptRegistrant<YaRRVehicle> YaRRVehicleRegistrant("YaRRVehicle", "");


void YaRRBuilding::Created(GameObject *o)
{
	DLOG;
	dwarning = 0;
	dswarning = 0;
	Commands->Start_Timer(o, this, 1.0, 1); 
}

void YaRRBuilding::Timer_Expired(GameObject *o, int number)
{
	DLOG;
	if(number == 1)
	{
		if((Commands->Get_Health(o)/Commands->Get_Max_Health(o)) == 1)
		{
			if(DamageList.Length() > 0)
			{
				IterateStack(dlt, ObjectDamaged *, (&DamageList))
				{
					Dealloc(ObjectDamaged, dlt);
				}
			}
			DamageList.Clear();
		}

		if((Commands->Get_Health(o)/Commands->Get_Max_Health(o))*100 < 10)
		{
			if(dwarning == 0)
			{
				const char *bld_snd = FindDISound(o);
				YaRRFunctions::ConsoleInputF("sndt %d %s", Get_Object_Type(o), bld_snd);
				dwarning = 1;
				Commands->Start_Timer(o, this, 20, 2);
			}
		}
		else if((Commands->Get_Health(o)/Commands->Get_Max_Health(o))*100 < 25)
		{
			if(dswarning == 0)
			{
				YaRRFunctions::ConsoleInputF("sndt %d %s", Get_Object_Type(o), (Get_Object_Type(o) == 0 ? "mxxdsgn_dsgn0037i1evan_snd.wav" : "mxxdsgn_dsgn0039i1evag_snd.wav"));
				dswarning = 1;
				Commands->Start_Timer(o, this, 20, 2);
			}
			
		}
	}
	if(number == 2)
	{
		if((Commands->Get_Health(o)/Commands->Get_Max_Health(o))*100 > 10)
		{
			dwarning = 0;
		}
		if((Commands->Get_Health(o)/Commands->Get_Max_Health(o))*100 > 25)
		{
			dswarning = 0;
		}
		else
		{
			Commands->Start_Timer(o, this, 20, 2);
		}
	}
	Commands->Start_Timer(o, this, 1.0, 1);
}
void YaRRBuilding::Damaged(GameObject *o, GameObject *attacker, float damage)
{
	DLOG;
	if(!attacker || !o || (o == attacker) || !Commands->Is_A_Star(attacker) || (damage == 0))
	{
		return;
	}
	Player *p = Find(Get_Player_ID(attacker));
	if(!p)
	{
		return;
	}
	int t = Get_Object_Type(o);
	if(t == p->data->PlayerType.Get() || t < 0 || t > 1)
	{
		return;
	}

	if(damage > 0)
	{
		p->Rank.Building_Damage += RankInfo::Building_Damage_CoEfficient * damage;
	}
	else
	{
		p->Rank.Building_Repair += RankInfo::Building_Repair_CoEfficient * damage;
	}
	YaRRVeteran::ObjectDamaged(p->ID, o, damage);

	int ID = Get_Player_ID(attacker);
	bool f = 0;
	IterateStack(obd, ObjectDamaged *, (&DamageList))
	{
		if(obd->PlayerID == ID)
		{
			f = 1;
			obd->Damage = damage + obd->Damage;
		}
	}
	if(!f)
	{
		ObjectDamaged *odmg = Alloc(ObjectDamaged);
		odmg->Damage = damage;
		odmg->PlayerID = ID;
		DamageList.Push(odmg);
	}
}

void YaRRBuilding::Killed(GameObject *o, GameObject *killer)
{
	DLOG;
	const char *pscreenname = GetPresetScreenName(o);
	char *pkilled = (char *)CAlloc(1);
	*pkilled = 0;
	float MHP = 0;

	IterateStack(od1, ObjectDamaged *, (&DamageList))
	{
		MHP += od1->Damage;
	}

	IterateStack(od2, ObjectDamaged *, (&DamageList))
	{
		if((od2->Damage / MHP) > 0.1)
		{
			char tmp[16];
			sprintf(tmp, "\x03%d%S (%.1f%%) ", Get_Object_Type(o) == 1 ? 4 : 8, Get_Wide_Player_Name_By_ID(od2->PlayerID), (od2->Damage / MHP)*100);
			pkilled = (char *)RAlloc(pkilled, strlen(pkilled)+strlen(tmp)+1);
			strcat(pkilled, tmp);

			YaRRVeteran::Kill(o, od2->PlayerID);
			YaRRVeteran::ObjectKilled(Get_Player_ID(killer), o);
			PPage(od2->PlayerID, "Congratulations on killing the %s", pscreenname);
			
			Player *p = Find(od2->PlayerID);
			if(!p)
			{
				continue;
			}
			p->Rank.Building_Kills++;
		}
		Dealloc(ObjectDamaged, od2);
	}
	const char *objp = GetPresetScreenName(o);
	IRC::SendC('b', "%sdestroyed the %s", pkilled, objp);
	CDealloc(objp);
	CDealloc(pscreenname);
	
}

void YaRRBuilding::Destroyed(GameObject *o)
{
	DLOG;

}

ScriptRegistrant<YaRRBuilding> YaRRBuildingRegistrant("YaRRBuilding", "");


void YaRRBeacon::Created(GameObject *o)
{
	DLOG;

}

void YaRRBeacon::Timer_Expired(GameObject *o, int number)
{
	DLOG;

}

void YaRRBeacon::Damaged(GameObject *o, GameObject *attacker, float damage)
{
	DLOG;
	if(!attacker || !Commands->Is_A_Star(attacker) || (damage == 0))
	{
		return;
	}
	Player *p = Find(Get_Player_ID(attacker));
	if(!p)
	{
		return;
	}

	p->Rank.Beacon_Damage += damage * RankInfo::Beacon_Damage_CoEfficient;
	YaRRVeteran::ObjectDamaged(p->ID, o, damage);
}

void YaRRBeacon::Killed(GameObject *o, GameObject *killer)
{
	DLOG;
	if(!Commands->Is_A_Star(killer))
	{
		return;
	}
	YaRRVeteran::Kill(o, Get_Player_ID(killer));
	YaRRVeteran::ObjectKilled(Get_Player_ID(killer), o);
}

void YaRRBeacon::Destroyed(GameObject *o)
{
	DLOG;

}

ScriptRegistrant<YaRRBeacon> YaRRBeaconRegistrant("YaRRBeacon", "");


void YaRRC4::Created(GameObject *o)
{
	DLOG;
	
}

void YaRRC4::Timer_Expired(GameObject *o, int number)
{
	DLOG;

}

void YaRRC4::Damaged(GameObject *o, GameObject *attacker, float damage)
{
	DLOG;
	if(!attacker || !Commands->Is_A_Star(attacker) || (damage == 0))
	{
		return;
	}
	Player *p = Find(Get_Player_ID(attacker));
	if(!p)
	{
		return;
	}
	p->Rank.C4_Damage = damage * p->Rank.C4_Damage_CoEfficient;
	YaRRVeteran::ObjectDamaged(Get_Player_ID(attacker), o, damage);
}

void YaRRC4::Killed(GameObject *o, GameObject *killer)
{
	DLOG;
	if(!Commands->Is_A_Star(killer))
	{
		return;
	}
	YaRRVeteran::Kill(o, Get_Player_ID(killer));
	YaRRVeteran::ObjectKilled(Get_Player_ID(killer), o);
}

void YaRRC4::Destroyed(GameObject *o)
{
	DLOG;

}

ScriptRegistrant<YaRRC4> YaRRC4Registrant("YaRRC4", "");


void YaRRRebuild::Created(GameObject *o)
{
	DLOG;
	
}

void YaRRRebuild::Destroyed(GameObject *o)
{

}

ScriptRegistrant<YaRRRebuild> YaRRRebuildRegistrant("YaRRRebuild", "");

void YaRRBeaconCtrl::Created(GameObject *o)
{
	DLOG;
	
}

void YaRRBeaconCtrl::Timer_Expired(GameObject *o, int number)
{
	DLOG;

}

ScriptRegistrant<YaRRBeaconCtrl> YaRRBeaconCtrlRegistrant("YaRRBeaconCtrl", "");


void YaRR_Kill::Created(GameObject *obj)
{
	DLOG;
	Commands->Start_Timer(obj, this, 0.1, 1);
	Commands->Start_Timer(obj, this, 0.2, 2);
}

void YaRR_Kill::Timer_Expired(GameObject *o, int number)
{
	DLOG;
	if(number == 1)
	{
		GameObject *veh = Get_Vehicle(o);
		if(!veh)
		{
			Commands->Apply_Damage(o, 9999.9, "BlamoKiller", NULL);
			Destroy_Script();
			return;
		}
		else
		{
			Commands->Apply_Damage(veh, 9999.9, "BlamoKiller", NULL);
		}
	}
	else if(number == 2)
	{
		Commands->Apply_Damage(o, 9999.9, "BlamoKiller", NULL);
		Destroy_Script();
		return;
	}
}

ScriptRegistrant<YaRR_Kill> YaRR_KillRegistrant("YaRRKill", "");

void YaRR_PlaySound::Created(GameObject *obj)
{
	Stacker<WavSound> *tmp = (Stacker<WavSound> *)Get_Int_Parameter("Sound");
	if(!tmp)
	{
		Destroy_Script();
		return;
	}

	CopyStack((&Sounds), tmp, WavSound);
	Sounds.Reset();
	Commands->Start_Timer(obj, this, 0.0, 0);
}

void YaRR_PlaySound::Timer_Expired(GameObject *obj, int Number)
{
	WavSound s;
	if(!Sounds.Iterate(&s))
	{
		Destroy_Script();
		return;
	}
	if(s.Public || Get_Int_Parameter("ID") == 0)
	{
		ConsoleInputF("snda %s", s.Name);
	}
	else
	{
		ConsoleInputF("sndp %d %s", Get_Int_Parameter("ID"), s.Name);
	}	
	Commands->Start_Timer(obj, this, s.Duration, 0);
	
}

ScriptRegistrant<YaRR_PlaySound> YaRR_PlaySoundRegistrant("YaRR_PlaySound", "Sound:int,ID:int");

void YaRRProtect::Created(GameObject *o)
{
	Player *p = Find(Get_Player_ID(o));
	if(!p)
	{
		return;
	}

	p->Protected = 1;
	Commands->Set_Is_Visible(o, 0);

}

void YaRRProtect::Destroyed(GameObject *o)
{
	Player *p = Find(Get_Player_ID(o));
	if(!p)
	{
		return;
	}

	p->Protected = 0;
}

void YaRRProtect::Custom(GameObject *o, int message, int param, GameObject *sender)
{
	if(message == 20)
	{
		Player *p = Find(Get_Player_ID(o));
		if(!p)
		{
			return;
		}

		p->Protected = 0;
		Commands->Set_Is_Visible(o, 1);
	}
}

ScriptRegistrant<YaRRProtect> YaRRProtectRegistrant("YaRRProtect", "");


void YaRRRoot::Created(GameObject *o)
{
	Player *p = Find(Get_Player_ID(o));
	if(!p)
	{
		return;
	}

	p->Rooted = 1;
	Vector3 position = Commands->Get_Position(o);
	position.Z -= 0.5;
	Commands->Set_Position(o, position);
}

void YaRRRoot::Destroyed(GameObject *o)
{
	Player *p = Find(Get_Player_ID(o));
	if(!p)
	{
		return;
	}

	p->Rooted = 0;
}

void YaRRRoot::Custom(GameObject *o, int message, int param, GameObject *sender)
{
	if(message == 21)
	{
		Player *p = Find(Get_Player_ID(o));
		if(!p)
		{
			return;
		}

		p->Rooted = 0;
		Vector3 position = Commands->Get_Position(o);
		position.Z += 1;
		Commands->Set_Position(o, position);
	}
}

ScriptRegistrant<YaRRRoot> YaRRRootRegistrant("YaRRRoot", "");

void YaRRVeteranScript::Created(GameObject *o)
{
	Player *p = YaRRPlayers::Find(Get_Player_ID(o));
	if(!p)
	{
		return;
	}
	if(p->Rank.Level == 0)
	{
		return;
	}
	Promote = YaRRSettings::GetPremotionInfo(p->Rank.Level-1);

	Set_Max_Health(o, Commands->Get_Health(o)*(((float)Promote->Health / (float)100)+1)); 
	Set_Max_Shield_Strength(o, Commands->Get_Shield_Strength(o)*(((float)Promote->Armor / (float)100)+1));

	IterateStack(x, char *, Promote->Powerups)
	{
		Grant_Powerup(o, x);
	}
	Commands->Start_Timer(o, this, 5.0, 1);
}

void YaRRVeteranScript::Timer_Expired(GameObject *o, int number)
{
	if(number == 1)
	{
		Commands->Start_Timer(o, this, 5.0, 1);
		int Grant = Promote->Regeneration;

		int Diffrence = (int)Commands->Get_Max_Health(o) - (int)Commands->Get_Health(o);
		if(Diffrence > Grant)
		{
			Commands->Set_Health(o, Commands->Get_Health(o)+Grant);
			return;
		}
		Commands->Set_Health(o, Commands->Get_Max_Health(o));
		Grant -= Diffrence;

		Commands->Set_Shield_Strength(o, Commands->Get_Shield_Strength(o) + Grant);
		
	}
}

ScriptRegistrant<YaRRVeteranScript> YaRRVeteranRegistrant("YaRRVeteran", "");

