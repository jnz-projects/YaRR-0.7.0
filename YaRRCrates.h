

/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/


class Crate
{
public:
	char Name[64];
	int ChanceDN;
	int ChanceUP;
	void (*Activate)(Player *);
	int CrateID;
	int BlockerID;
};

class YaRRCrate
{
	static void Lock();
	static void Unlock();
	static CRITICAL_SECTION Mutex;

	static bool IsActive;
public:
	static Stacker<Crate> *Crates;
	static Stacker<Vector3> *Spawns;
	static void Startup();
	static void Shutdown();
	static void ObjectCreated(GameObject *obj);
	static unsigned Timer(void *);

	static void Test(Player *p);
	

	class CrateScript : public ScriptImpClass
	{
		Crate ThisCrate;
		void Created();
		void Custom(GameObject *obj, int message, int param, GameObject *sender);
	};
};