

/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/


#define WORKER_RUNNING 0x1
#define WORKER_BUSY 0x2
#define WORKER_IDLE 0x4

struct Task
{
	unsigned int (*CALL)(void *);
	void *Param;
	char Name[32];
	unsigned int Count;
	
};
class YaRRWorker
{
	static Stacker<Task *> *Tasks;
	static DWORD __stdcall YaRRWorker::Worker(void *);
	static CRITICAL_SECTION Mutex;
	static void *WorkerObj;
public:
	static void Addtask(Task &t_);
	static void Startup();
	static void Shutdown();
	static char Flags();
};