

/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/


class YaRRphp : public YaRRLink
{
	static void *Thread;
	static CRITICAL_SECTION Mutex;
	static Stacker<Player> internPlayers;
public:

	static void Start();
	static void Stop();

	static DWORD __stdcall Worker(void *p);
	static void Think();
};