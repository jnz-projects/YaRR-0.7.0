

/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/


class TCPSocket
{
	SOCKET Socket;
	char Host[256];
	int Port;
	bool Connected;
	int Mode;

	TCPSocket(SOCKET &_Socket, int _Mode);

public:
	
	TCPSocket();
	TCPSocket(SOCKET &_Socket);
	TCPSocket(TCPSocket &_Socket);
	TCPSocket(const char *Host, int Port);
	TCPSocket(const char *IP, int Port, int Backlog);
	~TCPSocket();
	
	bool Client(const char *Host, int Port);
	bool Server(const char *IP, int Port, int Backlog);
	bool Accept(TCPSocket *NewConnection);

	bool Is_Connected();
	bool Is_DataAvaliable();
	bool Is_ConnectionWaiting();

	bool RecviveData(char *Data, int Length);
	bool SendData(const char *Data, int Length);

	bool Destroy();
};


struct User
{
	char Nick[256];
	char Modes;
};

struct ircChannel
{
	char Name[256];
	Stacker<User *> Users;
};

struct RConnect
{
	int count;
	char host[256];
};

struct ircdata
{
	Stacker<char *> EXTCommands;
	Stacker<ircChannel *> Channels;
	TCPSocket Sock;
	char Host[128];
	char Nick[128];
	char Backup_Nick[128];
	char NSPassword[128];	
	int Port;
	void (*Callback)(ircdata *, const char *);
	int ThinkScript;
	bool Ghost;
	bool Ping;
};

class IRC
{
	static char Buffer[1024];
	static int Position;
	static clock_t Activity;
public:
	static void Load();
	static void Unload();

	static const char *IRC::Connect(ircdata **Data, const char *Host, int Port, const char *Nick, const char *Backup_Nick, const char *NSPassword, Stacker<char *> *EXTCommands, void (*Callback)(ircdata *, const char *));
	static void Send(const char *Packet, ...);
	static void __stdcall SendC(char Type, const char *Message, ...);
	static void SendC(const char *Channel, const char *Packet, ...);
	static void SendALL(const char *Packet, ...);
	static void Disconnect(const char *Message, ...);
	static void Disconnect();
	static bool FindStatus(const char *Nick, const char *Channel, char *access);
	
	static void Think(GameObject *o);
};