

/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/


class YaRRVeteran : public YaRRLink
{
public:
	static void Startup();
	static void Shutdown();
	static void Kill(GameObject *Killed, int ID);
	static void Killed(GameObject *Killer, int ID);
	static void Purchased(const char *Preset, int Type, int ID);
	static void Think();
	static void GiveMedal(Player *p, int Type);
	static void GiveAward(Player *p, int Type);
	static void PlayerJoined(int ID);
	static void ObjectDamaged(int PlayerID, GameObject *obj, float Damage);
	static void ObjectKilled(int PlayerID, GameObject *obj);
	static void PlayerLeave(Player *p);
	static void Mapend();
	static void Promote(Player *p);
	static int GetLevel(int Score);
	static int GetScore(int Level);
};