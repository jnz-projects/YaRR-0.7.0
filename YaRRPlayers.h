

/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/



class YaRRPlayers
{
public:
	static void Startup();
	static void Shutdown();
	static Player *Find(int ID);
	static void GivePoints(int ID, int Points);
	static void Joined(int ID);
	static void Leave(int ID);
	static void MapLoad();
	static bool WasHeadShot(int ID);
	static Player *Find_By_Name(const char *Name);
	static void ThinkingJoin(const char *Nick);
	static void LoadingMap(const char *Nick, bool Reserved);
	static void YaRRPlayers::Lock();
	static void YaRRPlayers::Unlock();

	static CRITICAL_SECTION Mutex;
	static Stacker<char *> *PreJoin;
};





extern Player **Players;