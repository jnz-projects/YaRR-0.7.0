#include "YaRRIncludes.h"


/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/


_Trace Trace_;
_SetFunctions SetFunctions_;


void *alloc(int x)
{
#ifdef _YaRRDebug
	return YaRRMemory::Allocate2(x, 0, "YaRRTrace.dll");
#else
	return YaRRMemory::Allocate2(x);
#endif
}

void *ralloc(void *p, int x)
{
#ifdef _YaRRDebug
	return YaRRMemory::ReAllocate(p, x, 0, "YaRRTrace.dll");
#else
	return YaRRMemory::Allocate2(x);
#endif
}


void YaRRTrace::Startup()
{
	DLOG;
	HMODULE YaRRDll = LoadLibrary("YaRRTrace.dll");
	if(!YaRRDll)
	{
		YaRRFunctions::YaRRError("Unable to load YaRRTrace.dll");
	}
	SetFunctions_ = (_SetFunctions)GetProcAddress(YaRRDll, "SetFunctions");
	if(!SetFunctions_)
	{
		YaRRFunctions::YaRRError("Unable to load \"SetFunctions\" from YaRRTrace.dll");
	}
	Trace_ = (_Trace)GetProcAddress(YaRRDll, "Trace");
	if(!Trace_)
	{
		YaRRFunctions::YaRRError("Unable to load \"Trace\" from YaRRTrace.dll");
	}

	SetFunctions_((void *(*)(int))alloc, (void *(*)(void *,int))ralloc);
}

void YaRRTrace::Shutdown()
{
	DLOG;

}

const char *YaRRTrace::Trace(const char *Host, Stacker<char *> *Result)
{
	DLOG;
	return Trace_((char *)Host, Result);
}