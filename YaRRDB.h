

/************************************** Polite Request ***************************************
** I have put a lot of time and effort into YaRR. If you want to use some of the source     **
** please tell me. Anything used from YaRR must be open source, as is scripts.dll.          **
***************************************** Thank you *****************************************/



class YaRRDatabase
{
	//CREATE TABLE Players(Uid INTERGER PRIMARY KEY, ID INTERGER, Nick char(32), JOINMSG char(256));
	//CREATE TABLE Bans(UID INTERGER PRIMARY KEY, Nick TEXT, WhoBy TEXT, Reason TEXT, Time INTERGER);

	static sqlite3 *db;
	static int Callback(void *Data, int argc, char **ColumnData, char **Columns);

public:
	struct Column
	{
		char *Name;
		char *Data;
	};
	struct Row
	{
		Stacker<Column *> *Columns;
	};

	static void Startup();
	static void Shutdown();

	static const char *YaRRDatabase::Query(Stacker<YaRRDatabase::Row *> *l, const char *Format, ...);
	static void DeleteResult(Stacker<Row *> *list);
	static char *GetColumnData(const char *Name, Stacker<Column *> *Row);
};